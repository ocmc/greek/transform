# Transformer

Tool to transform Greek text

The current version of transformer simply removes accents and diacritics from polytonic and monotonic Greek text.

A future version will convert polytonic to monotonic.

## Installation for Windows
1. Go to the [releases page](https://gitlab.com/ocmc/greek/transform/-/releases)
2. The source code for the release is available as both a zip and tar file.  If you have go installed and know what you are doing, you can build the executable yourself.
3. Assuming you want to download the executable, click on transformer.exe
4. You will get a warning: transformer.exe is not commonly downloaded and may be dangerous.
5. Click on the down arrow to the right of the word ```Discard```.
6. Select ```Keep```
7. It will be in your downloads directory. You can move it into a directory that is on your Windows path.  Or, add the directory you put it into in your path.  See [this](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/).
8. If when you run it, you get a popup that says ```Windows protected your PC```, click on ```More info```.
9. The click ```Run anyway```

## Running It

To transform a short text:

```   transformer p2n --txt Θεὸς Κύριος καὶ ἐπέφανεν ἡμῖν. Εὐλογημένος ὁ ἐρχόμενος ἐν ὀνόματι Κυρίου.```

    The output will be:

    Θεος Κυριος και επεφανεν ημιν. Ευλογημενος ο ερχομενος εν ονοματι Κυριου.

To transform an entire file:

```   transformer p2n --file {path to the file}```

To transform all the files (non-recursively) in a directory:

```   transformer p2n --file {path to directory}```

For both the --file and --dir flags, the transformed files will be in the same directory, but have nwp_ as the file prefix.  If you want to rerun transformer in the same directory, be sure to first delete all the nwp_ prefixed files.

## Developer Notes

build.sh is for creating a 64-bit windows executable.  You will need to modify the output path for your purposes.