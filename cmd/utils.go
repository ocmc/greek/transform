package cmd

import (
	"bufio"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

// FileExists returns true if the final segment of the path exists and is not a directory.
func FileExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists returns true if the final segment of the path exists and is a directory
func DirExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// CreateDir will create a directory if it does not exist.
// The error will be nil unless something is wrong with the path.
func CreateDir(path string) error {
	var err error = nil
	if !DirExists(path) {
		err = os.Mkdir(path, os.ModePerm)
	}
	return err
}

// CreateDirs creates the specified directory and parents in path if they do not exist.
// The error will be nil unless something is wrong with the path.
func CreateDirs(path string) error {
	var err error = nil
	if !DirExists(path) {
		err = os.MkdirAll(path, os.ModePerm)
	}
	return err
}

// WriteFile writes the supplied content using the filename.
func WriteFile(filename, content string) error {
	f, err := os.Create(filename)
	defer f.Close()
	if err != nil {
		return err
	}
	w := bufio.NewWriter(f)
	_, err = w.WriteString(content)
	w.Flush()
	return err
}

// WriteLinesToFile writes the supplied lines to the specified filename.
func WriteLinesToFile(filename string, lines []string) error {
	f, err := os.Create(filename)
	defer f.Close()
	if err != nil {
		return err
	}
	for _, line := range lines {
		fmt.Fprintln(f, line)
	}
	f.Close()
	return err
}

// HTMLTemplateToFile parses the html template and writes it to the filename provided.
// name: the name of the template
// html: the template
// filename: the name of the html file including its path
// data: the data to be filled into the html (may be nil)
func HTMLTemplateToFile(name, html, filename string, data interface{}) error {
	t := template.Must(template.New(name).Parse(html))
	f, err := os.Create(filename)
	if err != nil {
		log.Println(fmt.Sprintf("create file %s: %s", filename, err))
		return err
	}
	err = t.Execute(f, data)
	if err != nil {
		log.Println(fmt.Sprintf("create template: %s", err))
		return err
	}
	f.Close()
	return nil
}

// HTMLTemplateToFile parses the html template and writes it to the filename provided.
// name: the name of the template
// html: the template
// filename: the name of the html file including its path
// data: the data to be filled into the html (may be nil)
func TextTemplateToFile(name, tmpl, filename string, data interface{}) error {
	t := template.Must(template.New(name).Parse(tmpl))
	f, err := os.Create(filename)
	if err != nil {
		log.Println(fmt.Sprintf("create file %s: %s", filename, err))
		return err
	}
	err = t.Execute(f, data)
	if err != nil {
		log.Println(fmt.Sprintf("create template: %s", err))
		return err
	}
	f.Close()
	return nil
}

// FileMatcher returns all files recursively in the dir that have the specified file extension and match the supplied regular expressions.
// The file extension should not have a leading dot.
// The expressions are regular expressions that will match the name of a file, but
// without the extension.  The extension will be automatically added to each expression.
// The expressions may be nil, in which case all file patterns will match.
func FileMatcher(dir, extension string, expressions []string) ([]string, error) {
	var result []string
	extensionPattern := "\\." + extension
	// precompile the expressions
	if expressions == nil {
		expressions = []string{".*"}
	}
	patterns := make([]*regexp.Regexp, len(expressions))
	for i, e := range expressions {
		p, err := regexp.Compile(e + extensionPattern)
		if err != nil {
			return result, err
		}
		patterns[i] = p
	}
	// now walk the files and apply the regular expressions
	err := filepath.Walk(ResolvePath(dir),
		func(path string, info os.FileInfo, err error) error {
			for _, p := range patterns {
				if p.MatchString(info.Name()) {
					result = append(result, path)
				}
			}
			return nil
		})
	return result, err
}

// EntriesInDir returns file info for each entry in the specified directory
func EntriesInDir(dir string) ([]os.FileInfo, error) {
	files, err := ioutil.ReadDir(dir)
	return files, err
}

// CopyEntriesInDir copies the contents of the source dir into the destination dir.
// The destination dir does not have to already exist.
// If verbose == true, fmt.Println will report each file and dir being copied
func CopyEntriesInDir(source, destination string, verbose bool) error {
	var err error
	if !DirExists(source) {
		return fmt.Errorf("directory %s does not exist", source)
	}
	if DirExists(destination) {
		files, err := EntriesInDir(source)
		if err != nil {
			return err
		}
		if verbose {
			fmt.Printf("\nCopying files and directories from %s to %s", source, destination)
		}
		for _, file := range files {
			if !strings.HasPrefix(file.Name(), ".") {
				from := filepath.Join(source, file.Name())
				to := filepath.Join(destination, file.Name())
				if verbose {
					fmt.Printf("\tCopying %s to %s\n", from, to)
				}
				if file.IsDir() {
					err := CopyDir(from, to)
					if err != nil {
						return err
					}
				} else {
					err := CopyFile(from, to)
					if err != nil {
						return err
					}
				}
			}
		}
		if verbose {
			fmt.Printf("Finished copying files and directories from %s to %s", source, destination)
		}
	} else {
		err = CopyDir(source, destination)
	}
	return err
}

// Returns the directory within which the caller is executing
// This is just an example.  It must exist as a function in
// the desired package.
func executionDir() string {
	_, filename, _, _ := runtime.Caller(0)
	dir, _ := path.Split(filename)
	return dir
}

// ResolvePath checks the path for a leading tilde and returns the expanded path.
// The tilde is replaced with the user's home dir path.
// If no tilde is present, the path is returned unmodified.
func ResolvePath(path string) string {
	usr, _ := user.Current()
	dir := usr.HomeDir
	var newPath = ""
	if path == "~" {
		newPath = dir
	} else if strings.HasPrefix(path, "~/") {
		// Use strings.HasPrefix so we don't match paths like
		// "/something/~/something/"
		newPath = filepath.Join(dir, path[2:])
	} else {
		newPath = path
	}
	return newPath
}

// GetFileLines opens the file at the specified path and returns the contents as a string
func GetFileContent(path string) (string, error) {
	b, err := ioutil.ReadFile(path) // just pass the file name
	if err != nil {
		return "", err
	}
	return string(b), nil // convert content to a 'string'
}

// GetFileLines opens the file at the specified path and returns an array of its lines
func GetFileLines(path string) ([]string, error) {
	readFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var fileTextLines []string

	for fileScanner.Scan() {
		fileTextLines = append(fileTextLines, fileScanner.Text())
	}

	readFile.Close()
	return fileTextLines, err
}

// CopyFile and CopyDir are from https://gist.githubusercontent.com/r0l1/92462b38df26839a3ca324697c8cba04/raw/dd62b7c2d425aef9db94ee0f4b772c621d09a845/copy.go
/* MIT License
 *
 * Copyright (c) 2017 Roland Singer [roland.singer@desertbit.com]
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// CopyFile copies the contents of the file named src to the file named
// by dst. The file will be created if it does not already exist. If the
// destination file exists, all it's contents will be replaced by the contents
// of the source file. The file mode will be copied from the source and
// the copied data is synced/flushed to stable storage.
func CopyFile(src, dst string) (err error) {
	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		if e := out.Close(); e != nil {
			err = e
		}
	}()

	_, err = io.Copy(out, in)
	if err != nil {
		return
	}

	err = out.Sync()
	if err != nil {
		return
	}

	si, err := os.Stat(src)
	if err != nil {
		return
	}
	err = os.Chmod(dst, si.Mode())
	if err != nil {
		return
	}

	return
}

// CopyDir recursively copies a directory tree, attempting to preserve permissions.
// Source directory must exist, destination directory must *not* exist.
// Symlinks are ignored and skipped.
func CopyDir(src string, dst string) (err error) {
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return fmt.Errorf("source is not a directory")
	}

	//_, err = os.Stat(dst)
	//if err != nil && !os.IsNotExist(err) {
	//	return
	//}
	//if err == nil {
	//	return fmt.Errorf("destination already exists")
	//}

	err = os.MkdirAll(dst, si.Mode())
	if err != nil {
		return
	}

	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())

		if entry.IsDir() {
			err = CopyDir(srcPath, dstPath)
			if err != nil {
				return
			}
		} else {
			// Skip symlinks.
			if entry.Mode()&os.ModeSymlink != 0 {
				continue
			}

			err = CopyFile(srcPath, dstPath)
			if err != nil {
				return
			}
		}
	}

	return
}
func Files(dir string) ([]string, error) {
	var files []string
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		return nil, err
	}
	return files, nil
}