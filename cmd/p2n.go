/*
Copyright © 2021 Michael Colburn (m.colburn@ocmc.org)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"os"
	"path/filepath"
	"strings"
	"unicode"
)

// p2nCmd represents the p2n command
var p2nCmd = &cobra.Command{
	Use:   "p2n",
	Short: "A brief description of your command",
	Long: `Removes accents and other diacritics from monotonic or polytonic Greek.
For example to convert text:
    transformer --p2n --txt Τοῦ πορεύεσθαι τὰ διαβήματά σου.
or to convert all the text in a file:
    transformer --p2n --file path/to/your/file.txt
or to convert the text in all files in a directory:
    transformer --p2n --dir path/to/directory 

When you convert a single file or ones in a directory, transform
will put the files in a directory called transformed.
`,
	Run: func(cmd *cobra.Command, args []string) {
		if isDir {
			dPath = args[0]
			if ! DirExists(dPath) {
				fmt.Printf("Directory %s not found.\n", dPath)
				os.Exit(1)
			}
			files, err := Files(dPath)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			for _, file := range files {
				err := processFile(file)
				if err != nil {
					fmt.Println(err)
				}
			}
		} else if isFile {
			fPath = args[0]
			if ! FileExists(fPath) {
				fmt.Printf("File %s not found.\n", dPath)
				os.Exit(1)
			}
			processFile(fPath)
		} else if isText {
			text = strings.Join(args, " ")
			if len(text) == 0 {
				fmt.Printf("You must provide the text to be transformed.")
				os.Exit(1)
			}
			fmt.Println(nwp(text))
		} else {
			fmt.Println("you must indicate the source of the text, i.e. --txt, --file, --dir")
			os.Exit(1)
		}
	},
}
func processFile(f string) error {
	fmt.Printf("Transforming %s\n", f)
	s, err := GetFileContent(f)
	if err != nil {
		return err
	}
	dir, file := filepath.Split(f)
	file = "nwp_" + file
	newPath := filepath.Join(dir, file)
	fmt.Printf("\tOutput is in %s\n", newPath)
	WriteFile(newPath, nwp(s))
	return nil
}
func nwp(in string) string {
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	s, _, _ := transform.String(t, in)
	return s
}
var isText bool
var isFile bool
var isDir bool
var dPath string
var fPath string
var text string

func init() {
	rootCmd.AddCommand(p2nCmd)
	p2nCmd.Flags().BoolVarP(&isText, "txt", "t", false, "transform text")
	p2nCmd.Flags().BoolVarP(&isFile,"file", "f", false, "transform file")
	p2nCmd.Flags().BoolVarP(&isDir, "dir", "d", false, "transform files in directory")
}
